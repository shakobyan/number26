package com.number26;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.number26.configuration.Number26Configuration;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Number26Configuration.class})
@WebAppConfiguration
public abstract class AbstractMvcTest {

    @Autowired
    private WebApplicationContext context;

    private ObjectMapper mapper = new ObjectMapper();

    public ResultActions put(String endpoint, TransactionRequestBuilder requestBuilder) throws Exception {

        String json = mapper.writeValueAsString(requestBuilder.build());

        return mockMvc().perform(MockMvcRequestBuilders.put(endpoint).content(json).accept(MediaType.APPLICATION_JSON)
                                                       .contentType(MediaType.APPLICATION_JSON));
    }

    public ResultActions get(String endpoint, TransactionRequestBuilder requestBuilder) throws Exception {

        String json = mapper.writeValueAsString(requestBuilder.build());

        return mockMvc().perform(MockMvcRequestBuilders.get(endpoint).content(json).accept(MediaType.APPLICATION_JSON)
                                                       .contentType(MediaType.APPLICATION_JSON));

    }

    private MockMvc mockMvc() {
        return webAppContextSetup(context).build();
    }

}