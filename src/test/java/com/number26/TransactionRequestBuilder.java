package com.number26;

import com.number26.model.TransactionDto;

public class TransactionRequestBuilder implements RequestBuilder {

    private final TransactionDto request;

    private TransactionRequestBuilder() {

        request = new TransactionDto();
        request.setAmount(50.5);
        request.setType("type");

    }

    public static TransactionRequestBuilder request() {
        return new TransactionRequestBuilder();
    }

    public TransactionRequestBuilder parentTransaction() {
        return this;
    }

    public TransactionRequestBuilder withParentId(Long parentId) {
        this.request.setParentId(parentId);
        return this;
    }

    public TransactionRequestBuilder withAmount(double amount) {
        this.request.setAmount(amount);
        return this;
    }

    @Override
    public Object build() {
        return request;
    }

}