package com.number26;

interface RequestBuilder {

    Object build();

}
