package com.number26.controller;

import com.number26.AbstractMvcTest;
import com.number26.enums.Status;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static com.number26.TransactionRequestBuilder.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransactionControllerTest extends AbstractMvcTest {

    private static final String REST_SERVICE_URI = "http://localhost:8080/transactionservice";
    private static final Long   TRANSACTION_ID_1 = 1L;
    private static final Long   TRANSACTION_ID_2 = 2L;
    private static final String TYPE = "type";


    @Test
    public void test1CreateTransaction() throws Exception {
        put(
                REST_SERVICE_URI + "/transaction/" + TRANSACTION_ID_1, request().parentTransaction()
        ).andExpect(
                status().isOk()
        ).andExpect(
                content().json("{status:" + Status.OK.value() + "}")
        );
    }

    @Test
    public void test2CreateTransaction_alreadyExists() throws Exception {
        put(
                REST_SERVICE_URI + "/transaction/" + TRANSACTION_ID_1, request().parentTransaction()
        ).andExpect(
                status().isBadRequest()
        ).andExpect(
                content().json("{status:" + Status.FAILURE.value() + "}")
        );
    }

    @Test
    public void test3GetTransaction_transactionExists() throws Exception {
        get(
                REST_SERVICE_URI + "/transaction/" + TRANSACTION_ID_1, request().parentTransaction()
        ).andExpect(
                status().isOk()
        );
    }

    @Test
    public void test4GetTransaction_transactionDoesNotExists() throws Exception {
        get(
                REST_SERVICE_URI + "/transaction/3", request().parentTransaction()
        ).andExpect(
                status().isNotFound()
        );
    }

    @Test
    public void test5CreateChildTransaction_withParentIdExists() throws Exception {
        put(
                REST_SERVICE_URI + "/transaction/2", request().withParentId(TRANSACTION_ID_1).withAmount(30.5)
        ).andExpect(
                status().isOk()
        );
    }

    @Test
    public void test6Sum_withChildren() throws Exception {
        get(
                REST_SERVICE_URI + "/sum/" + TRANSACTION_ID_1, request()
        ).andExpect(
                status().isOk()
        ).andExpect(
                content().json("{\"sum\":81.0}")
        );
    }

    @Test
    public void test7Sum_withhoutChildren() throws Exception {
        get(
                REST_SERVICE_URI + "/sum/" + TRANSACTION_ID_2, request()
        ).andExpect(
                status().isOk()
        ).andExpect(
                content().json("{\"sum\":30.5}")
        );
    }

    @Test
    public void test8GetTransactionIdsByType() throws Exception {
        get(
                REST_SERVICE_URI + "/types/" + TYPE, request()
        ).andExpect(
                status().isOk()
        ).andExpect(
                content().json("[1,2]")
        );
    }

}