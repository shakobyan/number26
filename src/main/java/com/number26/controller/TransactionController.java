package com.number26.controller;

import com.number26.entity.Transaction;
import com.number26.enums.Status;
import com.number26.model.CreateStatus;
import com.number26.model.SumDto;
import com.number26.model.TransactionDto;
import com.number26.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping(value = "/transactionservice", headers = "Accept=application/json")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(value = "/transaction/{transactionId}", method = PUT)
    public ResponseEntity<CreateStatus> createTransaction(
            @PathVariable("transactionId") long transactionId, @RequestBody TransactionDto transactionDto
    ) {
        Status createStatus = transactionService.saveTransaction(TransactionDto.to(transactionId, transactionDto));
        return new ResponseEntity<>(
                new CreateStatus(createStatus.value()),
                createStatus == Status.OK ? HttpStatus.OK : HttpStatus.BAD_REQUEST
        );
    }

    @RequestMapping(value = "/transaction/{transactionId}", method = RequestMethod.GET)
    public ResponseEntity<TransactionDto> getTransaction(@PathVariable("transactionId") long transactionId) {
        Transaction transaction = transactionService.findById(transactionId);
        return transaction != null ? new ResponseEntity<>(TransactionDto.from(transaction), HttpStatus.OK)
                                   : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/types/{type}", method = RequestMethod.GET)
    public ResponseEntity<List<Long>> getTransactionTypes(@PathVariable("type") String type) {
        List<Long> transactionIds = transactionService.getAmountsByTransactionType(type);

        return transactionIds.isEmpty() ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                                        : new ResponseEntity<>(transactionIds, HttpStatus.OK);
    }

    @RequestMapping(value = "/sum/{transactionId}", method = RequestMethod.GET)
    public ResponseEntity<SumDto> getTransactionAmountSummary(@PathVariable("transactionId") long transactionId) {
        Double sum = transactionService.getTransactionAmountSummary(transactionId);
        return sum != null ? new ResponseEntity<>(new SumDto(sum), HttpStatus.OK)
                           : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
