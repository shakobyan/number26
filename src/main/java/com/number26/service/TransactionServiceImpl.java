package com.number26.service;

import com.number26.entity.Transaction;
import com.number26.enums.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
class TransactionServiceImpl implements TransactionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionServiceImpl.class);

    private static List<Transaction> transactions;

    static {
        transactions = new ArrayList<>();
    }

    public Transaction findById(Long id) {
        if (id == null) {
            return null;
        }
        for (Transaction transaction : transactions) {
            if (transaction.getId() == id) {
                return transaction;
            }
        }
        return null;
    }

    public List<Long> getAmountsByTransactionType(String type) {
        List<Long> transactionIds = new ArrayList<>();
        transactions.stream().filter(t -> t.getType().equals(type)).forEach(t -> transactionIds.add(t.getId()));

        return transactionIds;
    }

    public Status saveTransaction(Transaction transaction) {
        Transaction trans = findById(transaction.getId());

        //Check if transaction exists
        if (trans != null) {
            LOGGER.error("transaction_id {} already exists.", transaction.getId());
            return Status.FAILURE;
        }

        //Check if transaction parent id is provided
        if (transaction.getParentId() != null && transaction.getParentId() != 0) {
            if (transaction.getParentId() == transaction.getId()) {
                LOGGER.error("transaction_id and parent_id are the same");
                return Status.FAILURE;
            }

            Transaction parentTransaction = findById(transaction.getParentId());

            if (parentTransaction != null) {
                parentTransaction.getChildren().add(transaction.getId());
                transactions.add(transaction);
            } else {
                LOGGER.error("transaction parent_id {} doesn't exist", transaction.getParentId());
                return Status.FAILURE;
            }
        } else {
            transactions.add(transaction);
        }
        return Status.OK;
    }

    public Double getTransactionAmountSummary(long transactionId) {

        Transaction transaction = findById(transactionId);
        if (transaction == null) {
            LOGGER.error("transaction with id {} doesn't exist", transactionId);
            return null;
        }

        double summary = transaction.getAmount();
        if (!transaction.getChildren().isEmpty()) {
            for (long id : transaction.getChildren()) {
                summary += getTransactionAmountSummary(id);
            }
        }
        return summary;
    }

}
