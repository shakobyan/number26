package com.number26.service;

import com.number26.entity.Transaction;
import com.number26.enums.Status;

import java.util.List;


public interface TransactionService {

    Transaction findById(Long id);

    List<Long> getAmountsByTransactionType(String type);

    Status saveTransaction(Transaction transaction);

    Double getTransactionAmountSummary(long transactionId);

}
