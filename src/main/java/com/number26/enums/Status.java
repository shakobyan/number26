package com.number26.enums;

public enum Status {

    OK("ok"),
    FAILURE("failure");

    private String status;

    Status(String status) {
        this.status = status;
    }

    public String value() {
        return status;
    }
}
