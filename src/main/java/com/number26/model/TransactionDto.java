package com.number26.model;

import com.number26.entity.Transaction;

public class TransactionDto {

    private double amount;
    private String type;
    private Long   parentId;


    public static TransactionDto from(Transaction transaction) {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setAmount(transaction.getAmount());
        transactionDto.setParentId(transaction.getParentId());
        transactionDto.setType(transaction.getType());
        return transactionDto;
    }

    public static Transaction to(long id, TransactionDto transactionDto) {
        Transaction transaction = new Transaction();
        transaction.setId(id);
        transaction.setAmount(transactionDto.getAmount());
        transaction.setType(transactionDto.getType());
        transaction.setParentId(transactionDto.getParentId());
        return transaction;
    }


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

}
