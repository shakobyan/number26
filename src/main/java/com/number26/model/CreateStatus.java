package com.number26.model;

public class CreateStatus {

    private String status;

    public CreateStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{status='" + status + '\'' + "}";
    }
}
