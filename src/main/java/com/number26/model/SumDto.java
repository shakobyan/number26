package com.number26.model;

public class SumDto {

    private double sum;

    SumDto() {}

    public SumDto(Double sum) {
        this.sum = sum;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }
}
